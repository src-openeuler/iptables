From 41139aee5e53304182a25f1e573f034b313f7232 Mon Sep 17 00:00:00 2001
From: Phil Sutter <phil@nwl.cc>
Date: Tue, 28 Nov 2023 20:21:49 +0100
Subject: libxtables: xtoptions: Fix for non-CIDR-compatible hostmasks

In order to parse the mask, xtopt_parse_hostmask() calls
xtopt_parse_plenmask() thereby limiting netmask support to prefix
lengths (alternatively specified in IP address notation).

In order to lift this impractical restriction, make
xtopt_parse_plenmask() aware of the fact that xtopt_parse_plen() may
fall back to xtopt_parse_mask() which correctly initializes val.hmask
itself and indicates non-CIDR-compatible masks by setting val.hlen to
-1.

So in order to support these odd masks, it is sufficient for
xtopt_parse_plenmask() to skip its mask building from val.hlen value and
take whatever val.hmask contains.

Fixes: 66266abd17adc ("libxtables: XTTYPE_HOSTMASK support")
Signed-off-by: Phil Sutter <phil@nwl.cc>

Conflict:NA
Reference:https://git.netfilter.org/iptables//commit/?id=41139aee5e53304182a25f1e573f034b313f7232

---
 libxtables/xtoptions.c | 5 +++++
 1 file changed, 5 insertions(+)

diff --git a/libxtables/xtoptions.c b/libxtables/xtoptions.c
index 0dcdf60..bc14958 100644
--- a/libxtables/xtoptions.c
+++ b/libxtables/xtoptions.c
@@ -714,6 +714,10 @@ static void xtopt_parse_plenmask(struct xt_option_call *cb)
 
 	xtopt_parse_plen(cb);
 
+	/* may not be convertible to CIDR notation */
+	if (cb->val.hlen == (uint8_t)-1)
+		goto out_put;
+
 	memset(mask, 0xFF, sizeof(union nf_inet_addr));
 	/* This shifting is AF-independent. */
 	if (cb->val.hlen == 0) {
@@ -734,6 +738,7 @@ static void xtopt_parse_plenmask(struct xt_option_call *cb)
 	mask[1] = htonl(mask[1]);
 	mask[2] = htonl(mask[2]);
 	mask[3] = htonl(mask[3]);
+out_put:
 	if (entry->flags & XTOPT_PUT)
 		memcpy(XTOPT_MKPTR(cb), mask, sizeof(union nf_inet_addr));
 }
-- 
2.33.0

